# Documentation

Elegant Roots se doit être structuré donc la documentation est centralisée. Du fonctionnel, par la technique au graphique, toutes les documentations sont ici.

## Sommaire

- [Fonctionnel](./fonctionnel)
- [Technique](./technique)
- [Graphique](./graphique)

## Auteurs

- [Orion Beauny-Sugot](https://gitlab.com/orion-beauny-sugot)
