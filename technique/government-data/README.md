# Government data

## What format ?

- [x] GeoJson
- [ ] Compressed GeoJson
- [ ] ShapeFile

## What they expose ?

- [x] Department
    - [x] Municipalities
    - Sections prefix
    - Sections
    - Pages
    - Plots
    - Building
    - Municipality
        - Municipalities
        - Sections prefix
        - Sections
          ```json
             {"id":"49007000AB","commune":"49007","prefixe":"000","code":"AB","created":"2002-10-07","updated":"2018-06-27"}
          ```
        - Pages
        - Plots
        - Building